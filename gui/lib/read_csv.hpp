#ifndef READ_CSV_PIERRE_HPP
#define READ_CSV_PIERRE_HPP


#include "tuple_tools/cat_tuples.hpp"
#include "tuple_tools/select_tuple.hpp"
#include "tuple_tools/filter_integer_sequence.hpp"


#include <array>
#include <string>
#include <istream>
#include <fstream>
#include <filesystem>
#include <unordered_map>

//--- usage ---
/*
std::vector<std::string> a,b;
auto rs = read_csv(
      "column_name1",  [&](const std::string &s){a.push_back(s);},
      "column_name2",  [&](const std::string &s){b.push_back(s);}
);
rs.sep_col=";";
rs.read("example.txt");
*/
    







namespace Read_csv_impl{

/*
template<typename indexes>
struct Extract_types;

template<size_t... I>
struct Extract_types< std::integer_sequence<size_t,I...> >{
  template<typename... A>
  static auto run(A&&... a){
    return  std::make_tuple( std::get< I  >( std::forward_as_tuple(a...) )... );
  }
};
*/



constexpr inline bool is_even(size_t x){return x%2==0;}
constexpr inline bool is_odd (size_t x){return x%2==1;}

inline void tokenize(std::vector<std::string> &write_here, const std::string & line, char sep){
  write_here.resize(0);
  
  if(line.size()==0){return;}
  write_here.emplace_back();
  
  for(const char c : line){
    if(c==sep ){write_here.emplace_back(); continue;}
    write_here.back()+=c;
  }

}


}

template<typename Functions_tuple, typename Sequence_even, typename Sequence_odd >
struct Read_csv_t;

template<typename Functions_tuple ,size_t... Even, size_t ... Odd>
struct Read_csv_t<
  Functions_tuple,
  std::integer_sequence<size_t,Even...> , 
  std::integer_sequence<size_t,Odd ...> 
> {
  static constexpr size_t size = std::tuple_size<Functions_tuple>::value;

  const Functions_tuple functions;
  const std::array<std::string,size> names;
  std::array<size_t,size>      indexes;
  
  char sep_col = '\t';
  char sep_lin = '\n';
  
  
  template<typename ...A > 
  Read_csv_t(A&&... a): 
    names    { std::get< Even  >( std::forward_as_tuple(a...) )...},
    functions( std::get< Odd   >( std::forward_as_tuple(a...) )...)
  {
    static_assert(size>0,"");
    static_assert(sizeof...(A)==2*size,"");
  }
  
  
  void read(std::istream &in);
  void read(const std::string            &p){std::ifstream f(p); if(!f){throw std::runtime_error("cannot read "+p);} read(f); }
  void read(const std::filesystem::path  &p){std::ifstream f(p); if(!f){throw std::runtime_error("cannot read "+p.string() );} read(f); }
  void read(const char * p)                 {std::ifstream f(p); if(!f){throw std::runtime_error("cannot read "+std::string(p) );} read(f); }
  
  private:
  template<size_t I = size-1>
  void line_r(const std::vector<std::string> &tokens){
    if constexpr(I>0){line_r<I-1>(tokens); }
    std::get<I>(functions)(  tokens[ std::get<I>(indexes )   ] );
  }
  
  
};


template<typename ...A >
auto read_csv( A&&... a){
  using namespace Read_csv_impl;
   
  using  all_num = std::make_integer_sequence<size_t,sizeof...(A) >;
  using even_num = typename tuple_tools::filter_integer_sequence<all_num, is_even >::type;
  using  odd_num = typename tuple_tools::filter_integer_sequence<all_num, is_odd  >::type;
  
  //using fn_tuple = decltype ( Extract_types<odd_num>::run(std::forward<A>(a)...) );
  using  fn_tuple = typename tuple_tools::select_tuple_elements<std::tuple<A...>, odd_num >::type ;
  Read_csv_t<fn_tuple,even_num, odd_num > r(std::forward<A>(a)...);
  
  return r;
}





template<typename Functions_tuple ,size_t... Even, size_t ... Odd>
void Read_csv_t<
  Functions_tuple,
  std::integer_sequence<size_t,Even...> , 
  std::integer_sequence<size_t,Odd ...> 
>::read(std::istream &in){
  using namespace Read_csv_impl;

  std::string line;
  std::vector<std::string> tokens;


  //first line : map indexes
  size_t max_index = 0;
  {
    std::unordered_map<std::string,size_t> col_map;
    getline(in,line,sep_lin);
    tokenize(tokens,line,sep_col); //this buffer is recycled every line
    for(size_t i = 0; i < tokens.size() ; ++i){
      if(tokens[i]==""){continue;}
      if(col_map.count(tokens[i])!=0){throw std::runtime_error("duplicated colname ,col_index="+std::to_string(i+1)+" name="+tokens[i]); }
      col_map[ tokens[i] ]=i;
    }
    
    for(size_t i = 0; i < size; ++ i ){
      auto f=col_map.find(names[i] );
      
      if(f== col_map.end() ){
        throw std::runtime_error("missing colname , name="+names[i]);
      }
      
      indexes[i]=f->second;
      if(f->second>max_index){max_index=f->second;}
    }
  
  
  }
  
  //content
  size_t line_count = 1;
  while(getline(in,line,sep_lin)){
    ++line_count;
    if(line==""){continue;}
    tokenize(tokens,line,sep_col);
    if(tokens.size()<=max_index){
      throw std::runtime_error("Not enough elements, line="+std::to_string(line_count)+", expected>="+std::to_string(max_index) + ", having="+std::to_string(tokens.size()) );
    }
    
    
    //call function for each function
    line_r(tokens);
    
  }
  
}

#endif
