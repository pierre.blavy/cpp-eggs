#ifndef THREAD_OPTIMIZE_MEAN_HPP
#define THREAD_OPTIMIZE_MEAN_HPP


#include <QThread>
#include <QEvent>
#include "widgets/MainWindow.hpp"


class Thread_log_event: public QEvent{
    public:
    explicit Thread_log_event(const QString &s):QEvent(QEvent::User),message(s){}
    QString message;
};


class Thread_optimize_mean: public QThread{
    Q_OBJECT
    public:
    explicit Thread_optimize_mean(MainWindow &mw, QObject* parent=nullptr):QThread(parent),main_window(mw){}
    virtual void run() override final;

    MainWindow & main_window;

    typedef double          value_type;
    std::vector<value_type> values;
    std::vector<size_t>     group_sizes;


    private :
    void log(const QString &s);
    void run_fn();



};





template<typename Container_v, typename Container_s, typename Fn>
void iterate_groups(const Container_v& values , const Container_s & groups, Fn fn) {
    //fn must be callable as (size_t group_id, begin, end), with begin end iterators to values

    typedef typename Container_v::value_type value_t;
    typedef typename Container_s::value_type group_size_t;

    static_assert(std::is_floating_point_v<value_t>, "The values must be floating point numbers" );
    static_assert(std::is_integral_v<group_size_t>,  "The group_sizes must be integral numbers" );

    auto b = values.begin();
    for(size_t group_id = 0; group_id < groups.size() ; ++ group_id){
        auto e = b + groups[group_id];
        fn(group_id, b, e);
        b=e;
    }
}
#endif
