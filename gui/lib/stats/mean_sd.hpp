#ifndef MEAN_SD_PIERRE_HPP
#define MEAN_SD_PIERRE_HPP

#include <math.h>

template<typename R=double, typename T>
inline R mean(T b, T e, size_t size  ){
   R s = std::accumulate(b,e, R(0) );
   s/=size;
   return s;
}

template<typename R=double, typename T>
inline R mean(T b, T e ){
  return mean<R>(b,e,std::distance(b,e) );
}





template<typename R=double, typename T>
inline auto sd_all(
  T b, 
  T e, 
  R m,  
  size_t size
){   
   R sd=0;
   for(auto x = b; x!=e; ++x){
     R tmp= ( m - *x );
     sd += tmp*tmp;
   }
   sd/=size;
   return sqrt(sd);
}


template<typename R=double, typename T>
inline auto sd_all(
  T b, 
  T e, 
  R m
){   
   return sd_all<R>(b,e,m, std::distance(b,e) );
}



template<typename R=double, typename T>
inline auto sd_sample(
  T b, 
  T e, 
  R m,  
  size_t size
){
   return sd_all<R>(b,e,m,size-1);
}

template<typename R=double, typename T>
inline auto sd_sample(
  T b, 
  T e, 
  R m
){
   return sd_all<R>(b,e,m, std::distance(b,e) );
}




#endif
