#ifndef QUANTILES_HPP
#define QUANTILES_HPP

//code adapted from : https://stackoverflow.com/questions/11964552/finding-quartiles


#include <algorithm>
#include <cmath>
#include <vector>
#include <array>
#include <type_traits>

/*
template<typename T>
static inline double Lerp(T v0, T v1, T t)
{
    return (1 - t)*v0 + t*v1;
}
*/



//in_sorted must be sorted
template<typename Rerturn_type=double, typename ContainerA, typename ContainerB>
static inline std::vector<Rerturn_type> quantile(const ContainerA& in_sorted, const ContainerB& probs){

    typedef typename ContainerA::value_type T;
    static_assert(std::is_floating_point_v<typename ContainerB::value_type > , "probs must be floating point numbers" );
    static_assert(std::is_floating_point_v<Rerturn_type > , "Rerturn_type must be floating point numbers" );

    auto Lerp=[](T v0, T v1, T t){
        return (1 - t)*v0 + t*v1;
    };


    if (in_sorted.empty())
    {
        return std::vector<Rerturn_type>();
    }

    if (1 == in_sorted.size())
    {
        return std::vector<Rerturn_type>(1, in_sorted[0]);
    }

    //std::vector<T> data = in_sorted;
    //std::sort(data.begin(), data.end());
    std::vector<Rerturn_type> quantiles;

    for (size_t i = 0; i < probs.size(); ++i)
    {
        T poi = Lerp(-0.5, in_sorted.size() - 0.5, probs[i]);

        size_t left  = std::max(size_t(std::floor(poi)), size_t(0));
        size_t right = std::min(size_t(std::ceil(poi)) , size_t(in_sorted.size() - 1));

        T datLeft = in_sorted.at(left);
        T datRight = in_sorted.at(right);

        T quantile = Lerp(datLeft, datRight, poi - left);

        quantiles.push_back(quantile);
    }

    return quantiles;
}



#endif // QUANTILES_HPP
