#ifndef BOXPLOT_H
#define BOXPLOT_H

#include <array>
#include <stdexcept>
#include <algorithm>

#include "quantiles.hpp"

template< typename ContainerA>
std::array<double,5> boxplot_r(const ContainerA& in_sorted){
    if(in_sorted.size() ==0){throw std::runtime_error("empty container in boxplot_r");}


    static const std::vector<double> v={0.25, 0.50 ,0.75 };
    auto q = quantile(in_sorted, v);

    auto iqr = q[2] - q[0];

    //https://www.r-bloggers.com/2012/06/whisker-of-boxplot/
    //upper whisker = min(max(x), Q_3 + 1.5 * IQR)
    //lower whisker = max(min(x), Q_1 – 1.5 * IQR)

    auto upper_whisker = std::min(in_sorted.back()  , q[2] + 1.5*iqr );
    auto lower_whisker = std::max(in_sorted.front() , q[0] - 1.5*iqr );

    return std::array<double,5>{
        lower_whisker, //lower whisker
        q[0],      //q25
        q[1],      //q50, median
        q[2],      //q75
        upper_whisker   //upper whisker
    };
}

template<typename T>
struct Boxplot_and_outliers{
    std::array<double,5> boxplot_data;
    std::vector<T>       outliers;
};

template< typename ContainerA>
auto boxplot_outliers_r(const ContainerA& in_sorted){
    typedef typename ContainerA::value_type value_type;
    Boxplot_and_outliers<value_type> r;

    r.boxplot_data = boxplot_r(in_sorted) ;

    for(const auto &x : in_sorted){
        if(x<r.boxplot_data[0]){r.outliers.push_back(x);}
        else{break;}
    }

    for(auto it = std::crbegin(in_sorted); it != std::crend(in_sorted); it++) {
        if(*it>r.boxplot_data[4]){r.outliers.push_back(*it);}
        else{break;}
    }

    return r;
}



#endif // BOXPLOT_H
