#include "BoxPlots.hpp"


#include <QtCharts/QChart>
#include <QtCharts/QBoxPlotSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QChartView>
//QT += charts
//https://doc.qt.io/qt-6/qboxplotseries.html
//dnf -y install qt6-qtcharts qt6-qtcharts-devel

#include <stats/boxplot.h>



BoxPlots::BoxPlots(QWidget *parent) : QChartView{parent} {

    chart = new QChart();
    this->setChart(chart);

    boxplots = new QBoxPlotSeries();
    chart->addSeries(boxplots);

    scatters = new QScatterSeries();
    chart->addSeries(scatters);

    BoxPlots::translate(false);

}

void BoxPlots::translate(bool){
    boxplots->setName(tr("Boxplots"));
    scatters->setName(tr("Outliers"));
}



void BoxPlots::clear(){
    is_first=true;
    xval=0;

    chart->removeAllSeries();

    boxplots = new QBoxPlotSeries();
    chart->addSeries(boxplots);

    scatters = new QScatterSeries();
    chart->addSeries(scatters);

    translate();

    //boxplots->clear();
    //scatters->clear();
}

void BoxPlots::addData      (      std::vector<double>&data ){std::sort(data.begin(), data.end()); addSortedData(data); }
void BoxPlots::addData      (const std::vector<double> &data){auto r=data; addData(r);}


void BoxPlots::addSortedData(const std::vector<double> &data){
    auto b = boxplot_outliers_r(data);

    //min max
    if(is_first){
        is_first=false;
        ymin=b.boxplot_data[0];
        ymax=b.boxplot_data[4];
    }else{
        set_min(ymin,b.boxplot_data[0]);
        set_max(ymax,b.boxplot_data[4]);
    }

    //boxplot
    auto boxplot   = new QBoxSet(b.boxplot_data[0],b.boxplot_data[1],b.boxplot_data[2],b.boxplot_data[3],b.boxplot_data[4]);
    boxplots->append(boxplot);

    //outliers min max
    for(const double x : b.outliers){
        set_min(ymin,x);
        set_max(ymax,x);
    }

    //scatterplot
    for(const double x : b.outliers){
        scatters->append(xval+0.5,x);
    }
    ++xval;

    //axes for scatterplots

    chart->createDefaultAxes();
    chart->axes(Qt::Vertical).at(0)->setMin(ymin);
    chart->axes(Qt::Vertical).at(0)->setMax(ymax);
    chart->axes(Qt::Vertical).at(0)->show();


    chart->axes(Qt::Horizontal).at(1)->setMin(0);
    chart->axes(Qt::Horizontal).at(1)->setMax(xval);
    chart->axes(Qt::Horizontal).at(1)->hide();
    //chart->axes(Qt::Vertical).at(1)->hide();


    /*
    chart->axes(Qt::Horizontal).at(1)->setMin(0);
    chart->axes(Qt::Horizontal).at(1)->setMax(xval+1);

    chart->axes(Qt::Vertical).at(1)->setMin(ymin);
    chart->axes(Qt::Vertical).at(1)->setMax(ymax);

    chart->axes(Qt::Vertical).at(0)->setMin(ymin);
    chart->axes(Qt::Vertical).at(0)->setMax(ymax);*/

}
