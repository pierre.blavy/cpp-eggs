#include "MainWindow.hpp"
#include <config/config.hpp>
#include <config/convert/convert_QString.hpp>


#include <QLabel>
#include <QPushButton>
#include <QPlainTextEdit>

#include <QtCharts/QChart>
#include <QtCharts/QBoxPlotSeries>
#include <QtCharts/QScatterSeries>
#include <QtCharts/QChartView>
  //QT += charts
  //https://doc.qt.io/qt-6/qboxplotseries.html
  //dnf -y install qt6-qtcharts qt6-qtcharts-devel

#include <widgets/Filepath.hpp>
#include <widgets/BoxPlots.hpp>


#include <qtt/qtt_layout.hpp>
#include <Thread_optimize_mean.hpp>

#include <stats/boxplot.h>



MainWindow::MainWindow(const config::Block &conf, QWidget* w):QMainWindow(w) {

    QWidget * main_widget = new QWidget;
    this->setCentralWidget(main_widget);

    QGridLayout * main_layout = qtt::new_layout<QGridLayout>();
    main_widget -> setLayout(main_layout);

    qtt::cstr_in_grid_span(in_label,main_layout ,1,1,1,2);

    qtt::cstr_in_grid(in_values_label,main_layout,2,1);
    qtt::cstr_in_grid(in_values_edit ,main_layout ,2,2);

    qtt::cstr_in_grid(in_groups_label,main_layout,3,1);
    qtt::cstr_in_grid(in_groups_edit ,main_layout ,3,2);

    qtt::cstr_in_grid_span(out_label,main_layout ,4,1,1,2);

    qtt::cstr_in_grid(out_folder_label,main_layout,5,1);
    qtt::cstr_in_grid(out_folder_edit ,main_layout ,5,2);

    qtt::cstr_in_grid_span(run_button,main_layout ,6,1,1,2);

    qtt::cstr_in_grid_span(result_label,main_layout ,7,1,1,2);
    qtt::cstr_in_grid_span(result_value,main_layout ,7,1,1,2);

    qtt::cstr_in_grid_span(boxplots,main_layout ,8,1,1,2);


    /*
    boxplot_chart=new QChart();
    boxplot->setChart(boxplot_chart);

    QBoxPlotSeries * ser = new QBoxPlotSeries();
    ser->setName("Values in each group");

    auto tmp1= new QBoxSet(0.1,0.5,1,1.5,1.75,"aa");

    std::vector<double> data;
    for(qreal a = 8; a < 10; a+=0.1){
        data.push_back(a);
    }
    std::sort(data.begin(),data.end() );
    auto tmp2_r = boxplot_outliers_r(data);
    auto tmp2   = new QBoxSet(tmp2_r.boxplot_data[0],tmp2_r.boxplot_data[1],tmp2_r.boxplot_data[2],tmp2_r.boxplot_data[3],tmp2_r.boxplot_data[4]);
    auto tmp2_s = new QScatterSeries;
    for(const auto &d : tmp2_r.outliers){  tmp2_s->append(1,d);}

    ser->append( tmp1);
    ser->append( tmp2);

    boxplot_chart->addSeries(ser);
    boxplot_chart->addSeries(tmp2_s);

    boxplot_chart->createDefaultAxes();
    //boxplot_chart->axes(Qt::Vertical).first()->setMin(0);
    //boxplot_chart->axes(Qt::Vertical).first()->setMax(10);

    boxplot_chart->axes(Qt::Horizontal).at(1)->setMin(0);
    boxplot_chart->axes(Qt::Horizontal).at(1)->setMax(10);


    boxplot_chart->legend()->setVisible(true);

    //boxplot_chart->axes(Qt::Horizontal).first()->setMax(11);
*/


    run_thread=new Thread_optimize_mean(*this, this);

    //--- configure widgets ---
    auto & conf_file=conf.blocks.get_unique("files");
    auto in_file=[&](Filepath*w, const std::string &key){
        w->set_must_exists();
        w->set_regular_file();
        w->setPath(conf_file.values.get_unique<QString>(key));
    };
    in_file(in_values_edit, "in_values");
    in_file(in_groups_edit, "in_groups");

    out_folder_edit->set_directory();
    out_folder_edit->set_must_exists();
    out_folder_edit->setPath(conf_file.values.get_unique<QString>("out_folder"));

    result_value->setReadOnly(true);

    //--- connect ---
    connect(run_button, &QPushButton::clicked, this, &MainWindow::on_click);
    connect(run_thread, &QThread::finished,    this, &MainWindow::on_finish);
    connect(run_thread, &QThread::started,    this, &MainWindow::on_start);

    translate(false);
}


void MainWindow::translate(bool tr_sons){
    in_label->setText("<b>"+tr("Input")+"</b>" );
    in_values_label->setText(tr("Values"));
    in_groups_label->setText(tr("Groups"));

    out_label->setText("<b>"+tr("Output")+"</b>" );
    out_folder_label->setText(tr("Out folder"));

    run_button->setText(tr("&Run","Keep R as shortcut"));
    result_label->setText("<b>"+tr("Result")+"</b>" );

    if(tr_sons){
        boxplots->translate();
    }
    //boxplots->setTitle(tr("Boxplot"));

}


void MainWindow::on_click(){
    if(run_thread->isRunning()){
        //try to interrupt
        run_button->setEnabled(false);
        run_button->setText(tr("Please wait..."));
        run_thread->requestInterruption();
    }else{
        //start the thread
        run_button->setEnabled(false);
        run_button->setText(tr("Starting..."));
        result_value->setPlainText("");
        run_thread->start();
    }
}

void MainWindow::on_start(){
    run_button->setText( tr("Interrupt") );
    run_button->setEnabled(true);
}


void MainWindow::on_finish(){
    run_button->setText(tr("Run again"));
    run_button->setEnabled(true);



    typedef std::vector<double>::const_iterator values_it;
    {//boxplots
        boxplots->clear();
        std::vector<double> sorted_data;
        iterate_groups(run_thread->values, run_thread->group_sizes, [&](size_t , values_it b, values_it e){
            //todo optimize : everything can be done inplace
            sorted_data.resize(0);
            for( ; b!=e; ++b ){
                sorted_data.push_back(*b);
            }
            std::sort(sorted_data.begin(),sorted_data.end() );
            boxplots->addSortedData(sorted_data);
        });
    }

}




bool MainWindow::event(QEvent *event){

    //QEvent::User

    if (event->type() == QEvent::User) {
        Thread_log_event *ke = dynamic_cast<Thread_log_event *>(event);
        if (ke!=nullptr){
            result_value->appendPlainText(ke->message);
            return true;
        }
    }

    return QWidget::event(event);
}


