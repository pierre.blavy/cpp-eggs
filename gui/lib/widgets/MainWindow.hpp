#ifndef MAINWINDOW_HPP
#define MAINWINDOW_HPP

#include <QMainWindow>

#include <qtt/qtt_Translatable.hpp>


namespace config{
  class Block;
}

class QPushButton;
class QLabel;
class QPlainTextEdit;
class QCoreApplication;
class Filepath;
class Thread_optimize_mean;

/*
class QChartView;
class QChart;
class QBoxPlotSeries;
*/

class BoxPlots;

class MainWindow: public QMainWindow, qtt::Translatable{
  Q_OBJECT
  public:

  MainWindow(const config::Block &conf, QWidget* w=nullptr);
  void translate(bool tr_sons=true)override final;

  bool event(QEvent *event)override;


  private:
  QLabel   * in_label = nullptr;

  QLabel   * in_values_label = nullptr;
  Filepath * in_values_edit  = nullptr;

  QLabel   * in_groups_label = nullptr;
  Filepath * in_groups_edit  = nullptr;

  QLabel   * out_label = nullptr;

  QLabel   * out_folder_label = nullptr;
  Filepath * out_folder_edit  = nullptr;

  QPushButton * run_button = nullptr;

  QLabel         * result_label = nullptr;
  QPlainTextEdit * result_value =nullptr;

  BoxPlots * boxplots = nullptr;

  /*
  QChartView     * boxplot       =nullptr; //widget
  QChart         * boxplot_chart =nullptr; //plot
  QBoxPlotSeries * boxplot_series=nullptr; //data and boxplot
  */

  Thread_optimize_mean * run_thread;

  void on_click();
  void on_finish();
  void on_start();


  friend Thread_optimize_mean;

};


#endif
