#ifndef BOXPLOTS_HPP
#define BOXPLOTS_HPP

#include <QChartView>
#include <qtt/qtt_Translatable.hpp>

class QBoxPlotSeries;
class QScatterSeries;
class QChart;

class BoxPlots : public QChartView, qtt::Translatable{
    Q_OBJECT
    public:
    explicit BoxPlots(QWidget *parent = nullptr);
    void translate(bool tr_sons=true)override;

    void addSortedData(const std::vector<double> &data);
    void addData      (       std::vector<double>&data);
    void addData      (const std::vector<double> &data);

    void clear();


    bool is_first=true;
    double  ymin, ymax;
    double  xval=0;

    QBoxPlotSeries * boxplots;
    QScatterSeries * scatters;

    QChart         * chart =nullptr; //plot

    static void set_min(double &d, const double &new_value){if(new_value<d){d=new_value;} }
    static void set_max(double &d, const double &new_value){if(new_value>d){d=new_value;} }


};

#endif // BOXPLOTS_HPP
