#ifndef FILEPATH_PIERRE_H
#define FILEPATH_PIERRE_H

#include <QWidget>
#include <qtt/qtt_Translatable.hpp>


class QLineEdit;
class QPushButton;
class IsOk;


class Filepath:public QWidget,qtt::Translatable{
    Q_OBJECT
    public:

    explicit Filepath (QWidget*parent=nullptr);
    void translate(bool tr_sons=true)override final;

    void set_regular_file(){file_type=FILE;      on_change();}
    void set_directory   (){file_type=DIRECTORY; on_change();}

    void set_must_exists()    {exist_type=MUST_EXISTS;     on_change();}
    void set_must_not_exists(){exist_type=MUST_NOT_EXISTS; on_change();}
    void set_warn_if_exists() {exist_type=WARN_IF_EXISTS;  on_change();}
    void set_do_not_care()    {exist_type=DO_NOT_CARE;     on_change();}

    QString path()const;
    void setPath(const QString &p);

    int status()const;

    signals:
    void pathChanged(QString);
    void statusChanged(int);


    //todo is_ok() const; is_error()const; is_warning()const; that delegate to isok


    private:
    QLineEdit   * value_edit    = nullptr;
    QPushButton * browse_button = nullptr;
    IsOk        * isok          = nullptr;

    void on_change();
    void on_browse();

    enum Filetype_e{DIRECTORY, FILE};
    Filetype_e file_type = FILE;

    enum Exist_e{MUST_EXISTS, MUST_NOT_EXISTS,WARN_IF_EXISTS,DO_NOT_CARE};
    Exist_e exist_type = MUST_EXISTS;

    QString last_value;
};



#endif // FILEPATH_PIERRE_H
