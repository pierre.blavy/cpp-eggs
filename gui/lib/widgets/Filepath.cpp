#include "Filepath.hpp"
#include <qtt/qtt_layout.hpp>

#include <QLineEdit>
#include <QPushButton>
#include <QFileInfo>
#include <QFileDialog>
#include <widgets/IsOk.hpp>

Filepath::Filepath (QWidget*parent):QWidget(parent){

    auto * main_layout = new QHBoxLayout();
    this->setLayout(main_layout);
    main_layout->setSpacing(0);

    qtt::cstr_in_layout(value_edit,   main_layout);
    qtt::cstr_in_layout(browse_button,main_layout);
    qtt::cstr_in_layout(isok,         main_layout);

    browse_button->setText("...");
    browse_button->setSizePolicy(QSizePolicy::Minimum  ,QSizePolicy::Minimum );
    browse_button->setStyleSheet("padding: 3px;");

    value_edit   ->setSizePolicy(QSizePolicy::Expanding,QSizePolicy::Minimum );

    connect(value_edit,&QLineEdit::textChanged,this,[this](const QString&){on_change();});
    connect(browse_button,&QPushButton::clicked,this,[this](bool){on_browse();});

    connect(isok,&IsOk::valueChanged,this,[&](IsOk::Status_e x){emit statusChanged(static_cast<int>(x));});

    translate(false);
}


void Filepath::translate(bool){
    browse_button->setToolTip(tr("Browse..."));
    on_change();
}

int Filepath::status()const{return isok->value();}


QString Filepath::path()const{
    //if( isok->is_error() ){return std::nullopt;}
    return last_value;
}

void Filepath::setPath(const QString &p){
    value_edit->setText(p);
}

void Filepath::on_browse(){

    QString title;
    if(file_type==FILE  ){
        if     (exist_type==MUST_EXISTS){      title=tr("Chose a file that exists"); }
        else if(exist_type==MUST_NOT_EXISTS){  title=tr("Choose a new file"); }
        else{                                  title=tr("Choose a file"); }
    }

    if(file_type==DIRECTORY){
        if     (exist_type==MUST_EXISTS){      title=tr("Chose a directory that exists"); }
        else if(exist_type==MUST_NOT_EXISTS){  title=tr("Choose a new directory"); }
        else{                                  title=tr("Choose a directory"); }
    }

    QString file_name;
    QString search_path="./";
    QString filter="File (*.*)";

    if(file_type==FILE  ){
        if     (exist_type==MUST_EXISTS){  file_name=QFileDialog::getOpenFileName(this, title, search_path , filter); }
        else                            {  file_name=QFileDialog::getSaveFileName(this, title, search_path , filter); }
    }

    if(file_type==DIRECTORY  ){
        file_name=QFileDialog::getExistingDirectory(this, title, search_path );
    }

    if(file_name!=""){
        value_edit->setText(file_name);
    }

}


void Filepath::on_change(){
    QFileInfo info(value_edit->text());

    bool err  = false;   QString err_s;
    bool warn = false;   QString warn_s;

    auto add_err =[&](const QString &msg){if( err_s!=""){ err_s+="\n";} err_s+=msg; err=true;  };
    auto add_warn=[&](const QString &msg){if(warn_s!=""){warn_s+="\n";}warn_s+=msg; warn=true;  };

    if(file_type==FILE      and !info.isFile()){ add_err(tr("A regular file is expected")); }
    if(file_type==DIRECTORY and !info.isDir()) { add_err(tr("A directory is expected"));  }


    if(file_type==FILE ){
        if(exist_type==MUST_EXISTS     and !info.exists() ){add_err(tr("The file must exist"));}
        else if(exist_type==MUST_NOT_EXISTS and  info.exists() ){add_err(tr("The file must NOT exist"));}
        else if(exist_type==WARN_IF_EXISTS  and  info.exists() ){add_warn(tr("The file already exist"));}
    }

    if(file_type==DIRECTORY ){
        if(exist_type==MUST_EXISTS     and !info.exists() ){add_err(tr("The directory must exist"));}
        else if(exist_type==MUST_NOT_EXISTS and  info.exists() ){add_err(tr("The directory must NOT exist"));}
        else if(exist_type==WARN_IF_EXISTS  and  info.exists() ){add_warn(tr("The directory already exist"));}
    }




    if(err){
        isok->set_error(err_s);
    }else if(warn){
        isok->set_warning(warn_s);
    }else{
        isok->set_ok();
    }




    //tooltip
    QString tt;
    auto add_tt =[&](const QString &msg){if( tt!=""){ tt+="\n";} tt+=msg; };
    const QString cannonical_path=info.canonicalFilePath();

    if(file_type==FILE  ){
        if     (exist_type==MUST_EXISTS){      add_tt(tr("A regular file that exists is expected.")); }
        else if(exist_type==MUST_NOT_EXISTS){  add_tt(tr("A regular file that do NOT exist is expected.")); }
        else{                                  add_tt(tr("A regular file is expected.")); }

        if(cannonical_path!=""){add_tt(tr("File path:")+cannonical_path) ;}
    }

    if(file_type==DIRECTORY){
        if     (exist_type==MUST_EXISTS)    { add_tt(tr("A directory that exists is expected.")); }
        else if(exist_type==MUST_NOT_EXISTS){ add_tt(tr("A directory that do NOT exist is expected.")); }
        else                                { add_tt(tr("A directory is expected.")); }
        if(cannonical_path!=""){add_tt(tr("Directory path:")+cannonical_path) ;}
    }

    value_edit->setToolTip(tt);



    //emit valueChanged
    if(cannonical_path!= last_value ){
        last_value=cannonical_path;
        emit pathChanged(cannonical_path);
    }

}
