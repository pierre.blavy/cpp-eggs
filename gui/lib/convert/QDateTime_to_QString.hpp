#ifndef QDATETIME_TO_QString_HPP
#define QDATETIME_TO_QString_HPP

//WARNING edit QDateTime_to_string.hpp consistently with this file


#include <convert/convert.hpp>
#include <convert/to_QString.hpp>

#include <str/to_digits.hpp>

#include <QString>
#include <QDateTime>
#include <QTimeZone>


template<>
struct Convert_t<QString, QDate ,void>{
    static constexpr bool is_implemented=true;
    typedef QString  To_t;
    typedef QDate    From_t;

    static To_t run(const From_t &f){
        return
              str::to_digits<QString>( f.year()        , 4 ) + "-"
            + str::to_digits<QString>( f.month()       , 2 ) + "-"
            + str::to_digits<QString>( f.day() , 2 )
            ;
    }
};


template<>
struct Convert_t<QString, QTime ,void>{
    static constexpr bool is_implemented=true;
    typedef QString  To_t;
    typedef QTime    From_t;

    static To_t run(const From_t &f){
        return
              str::to_digits<QString>( f.hour()   , 2 ) + ":"
            + str::to_digits<QString>( f.minute() , 2 ) + ":"
            + str::to_digits<QString>( f.second() , 2 ) + "."
            + str::to_digits<QString>( f.msec()   , 3 )
            ;
    }
};


template<>
struct Convert_t<QString, QTimeZone ,void>{
    static constexpr bool is_implemented=true;
    typedef QString    To_t;
    typedef QTimeZone  From_t;

    static To_t run(const From_t &f){
        return  f.id();
    }
};



template<>
struct Convert_t<QString, QDateTime ,void>{
    static constexpr bool is_implemented=true;
    typedef QString   To_t;
    typedef QDateTime From_t;

    static To_t run(const From_t &dt){
        return
              convert<QString>(dt.date()) + " "
            + convert<QString>(dt.time()) + " "
            + convert<QString>(dt.timeZone());
    }
};




#endif // QDATETIME_TO_QString_HPP
