#ifndef FROM_QSTRING_HPP
#define FROM_QSTRING_HPP

#include <QString>
#include <string>
#include <convert/convert.hpp>


#define GENERATE_CONVERT(type,fn) \
template<> \
struct Convert_t<type, QString ,void>{ \
    static constexpr bool is_implemented=true; \
    typedef type      To_t; \
    typedef QString   From_t; \
    static To_t run(const From_t &f){ \
        bool b; \
        auto r =  f.fn(&b); \
        if(!b){throw std::runtime_error("Cannot convert QString to " #type ", value="+f.toStdString());} \
        return r; \
    } \
}; \

//end macro



GENERATE_CONVERT(short,toShort)
GENERATE_CONVERT(ushort,toUShort)
GENERATE_CONVERT(int ,toInt)
GENERATE_CONVERT(uint,toUInt)
GENERATE_CONVERT(long ,toLong)
GENERATE_CONVERT(ulong,toULong)
GENERATE_CONVERT(qlonglong ,toLongLong)
GENERATE_CONVERT(qulonglong,toULongLong)

GENERATE_CONVERT(float,toFloat)
GENERATE_CONVERT(double,toDouble)

#undef GENERATE_CONVERT


template<>
struct Convert_t<std::string, QString ,void>{
        static constexpr bool is_implemented=true;
        typedef std::string  To_t;
        typedef QString      From_t;
        static To_t run(const From_t &f){
            return f.toStdString();
        }
};




/*
    short toShort(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<short>(*this, ok, base); }
    ushort toUShort(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<ushort>(*this, ok, base); }
    int toInt(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<int>(*this, ok, base); }
    uint toUInt(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<uint>(*this, ok, base); }
    long toLong(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<long>(*this, ok, base); }
    ulong toULong(bool *ok=nullptr, int base=10) const
    { return toIntegral_helper<ulong>(*this, ok, base); }
    QT_CORE_INLINE_SINCE(6, 5)
    qlonglong toLongLong(bool *ok=nullptr, int base=10) const;
    QT_CORE_INLINE_SINCE(6, 5)
    qulonglong toULongLong(bool *ok=nullptr, int base=10) const;
    float toFloat(bool *ok=nullptr) const;
    double toDouble(bool *ok=nullptr) const;
*/



#endif // FROM_QSTRING_HPP
