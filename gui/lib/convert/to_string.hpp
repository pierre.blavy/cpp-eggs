#ifndef TO_STRING_HPP
#define TO_STRING_HPP

#include <convert/convert.hpp>

#include <string>

template< typename From_tt>
struct Convert_t<std::string, From_tt ,void>{
    static constexpr bool is_implemented=true;
    typedef std::string  To_t;
    typedef From_tt      From_t;

    static To_t run(const From_t &f){
        return std::to_string(f);
    }
};



#endif // TO_STRING_HPP
