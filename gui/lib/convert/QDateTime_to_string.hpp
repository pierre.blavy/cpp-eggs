#ifndef QDATETIME_TO_STRING_HPP
#define QDATETIME_TO_STRING_HPP

//WARNING edit QDateTime_to_std::string.hpp consistently with this file


#include <convert/convert.hpp>
#include <convert/to_string.hpp>
#include <str/to_digits.hpp>

#include <string>
#include <QDateTime>


template<>
struct Convert_t<std::string, QDate ,void>{
    static constexpr bool is_implemented=true;
    typedef std::string  To_t;
    typedef QDate    From_t;

    static To_t run(const From_t &f){
        return
              str::to_digits<std::string>( f.year()        , 4 ) + "-"
            + str::to_digits<std::string>( f.month()       , 2 ) + "-"
            + str::to_digits<std::string>( f.day() , 2 )
            ;
    }
};


template<>
struct Convert_t<std::string, QTime ,void>{
    static constexpr bool is_implemented=true;
    typedef std::string  To_t;
    typedef QTime    From_t;

    static To_t run(const From_t &f){
        return
              str::to_digits<std::string>( f.hour()   , 2 ) + ":"
            + str::to_digits<std::string>( f.minute() , 2 ) + ":"
            + str::to_digits<std::string>( f.second() , 2 ) + "."
            + str::to_digits<std::string>( f.msec()   , 3 )
            ;
    }
};


template<>
struct Convert_t<std::string, QTimeZone ,void>{
    static constexpr bool is_implemented=true;
    typedef std::string    To_t;
    typedef QTimeZone  From_t;

    static To_t run(const From_t &f){
        return  f.id().toStdString();
    }
};



template<>
struct Convert_t<std::string, QDateTime ,void>{
    static constexpr bool is_implemented=true;
    typedef std::string   To_t;
    typedef QDateTime From_t;

    static To_t run(const From_t &dt){
        return
              convert<std::string>(dt.date()) + " "
            + convert<std::string>(dt.time()) + " "
            + convert<std::string>(dt.timeZone());
    }
};



#endif // QDATETIME_TO_STRING_HPP
