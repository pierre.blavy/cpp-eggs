#ifndef TO_QSTRING_HPP
#define TO_QSTRING_HPP

#include "convert.hpp"
#include <QString>
#include <string>


//--- const char* and std::string ---

template<>
struct Convert_t<QString, const char *,void>{
    static constexpr bool is_implemented=true;
    typedef QString      To_t;
    typedef const char * From_t;

    static To_t run(const From_t &f){
        return QString(f);
    }
};


template<>
struct Convert_t<QString, std::string ,void>{
    static constexpr bool is_implemented=true;
    typedef QString     To_t;
    typedef std::string From_t;

    static To_t run(const From_t &f){
        return QString::fromStdString(f);
    }
};


//--- default : fallback to arg ---
template<typename T>
struct Convert_t<QString, T ,void>{
    static constexpr bool is_implemented=true;
    typedef QString     To_t;
    typedef T           From_t;

    static To_t run(const From_t &f){
        return QString("%1").arg(f);
    }
};


#endif // TO_QSTRING_HPP
