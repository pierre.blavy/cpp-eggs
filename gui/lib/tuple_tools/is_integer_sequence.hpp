#ifndef IS_INTEGER_SEQUENCE
#define IS_INTEGER_SEQUENCE

#include <utility>


namespace tuple_tools{

template<typename T> struct is_integer_sequence{static constexpr bool value = false;}

template<typename T, T...a> 
struct is_integer_sequence< std::integer_sequence<T,a...> > {static constexpr bool value = true;}

}

#endif
