#ifndef SELECT_TUPLE_PIERRE_HPP
#define SELECT_TUPLE_PIERRE_HPP

namespace tuple_tools{



template<typename indexes>
struct select_types;

template<size_t... I>
struct Extract_types< std::integer_sequence<size_t,I...> >{
  template<typename... A>
  static auto run(A&&... a){
    return  std::make_tuple( std::get< I  >( std::forward_as_tuple(a...) )... );
  }
};


}
#endif
