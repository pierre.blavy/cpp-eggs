#ifndef CAT_INTEGER_SEQUENCE_PIERRE_HPP
#define CAT_INTEGER_SEQUENCE_PIERRE_HPP

#include <utility>



namespace tuple_tools{



template<typename... S> struct cat_integer_sequence;

// 1
template<typename T, T... a>
struct cat_integer_sequence< std::integer_sequence<T,a...> >{ 
  typedef std::integer_sequence<T,a...> type;
};


// 2+
template<typename T, T...a, T...b, typename... A>
struct cat_integer_sequence<std::integer_sequence<T,a...>, std::integer_sequence<T,b...>, A... >{
  typedef typename cat_integer_sequence<  std::integer_sequence<T,a...,b...> , A...>::type type;
};

}

#endif
