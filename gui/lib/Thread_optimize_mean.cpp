//=== This file is a modified version based on optimize_mean.hpp ===
//changed in a way, that makes it compatible with QThread.
//QThread doesn't use a std::strop_source to stop, instead it calls this->isInterruptionRequested()

#include "Thread_optimize_mean.hpp"

#include <optimize_mean.hpp>

#include <stats/mean_sd.hpp>
#include <stats/student.hpp>

#include <read_csv.hpp>
#include <boost/lexical_cast.hpp>

#include <QApplication>
#include <QMainWindow>

#include <widgets/Filepath.hpp>
#include <widgets/BoxPlots.hpp>

#include <convert/from_QString.hpp>





void Thread_optimize_mean::run(){
    try{
        run_fn();
    }catch(std::runtime_error &e){
        log(tr("ERROR:\n") + e.what());
    }catch(...){
        log(tr("ERROR : unknown error\n"));
    }
}


void Thread_optimize_mean::run_fn(){

  //load input files
  typedef std::vector<value_type>::const_iterator values_it;
    
  //std::vector<value_type> values;
  //std::vector<size_t>     group_sizes;
  values.clear();
  group_sizes.clear();

  //--- config goes here ---
  const std::string in_values  = main_window.in_values_edit ->path().toStdString();
  const std::string in_groups  = main_window.in_groups_edit ->path().toStdString();
  const std::string out_folder = main_window.out_folder_edit->path().toStdString();
  if(out_folder==""){throw std::runtime_error("Invalid out folder");}

  std::map<float,std::vector<std::string>> id_map;



  
  {// parse values.txt
    this->log(tr("Read values")+" ");
    auto rs = read_csv(
      "value",  [&](const std::string &s){values.emplace_back( boost::lexical_cast<double>(s) );},
        "id"   ,[&](const std::string &s){id_map[values.back()].emplace_back(s);}
    );
    rs.read(in_values);
    if(values.size()==0){throw std::runtime_error("no values, check "+in_values);}
    this->log(tr("OK")+"\n");
  }
  
  {// parse groups.txt
    this->log(tr("Read groups")+" ");
    auto rs = read_csv(
      "group_size",  [&](const std::string &s){group_sizes.emplace_back(boost::lexical_cast<size_t>(s));}
    );
    rs.read(in_groups);
    if(group_sizes.size()==0){throw std::runtime_error("no groups, check "+in_groups);}
    this->log("OK\n");
  }
  
  
  //run optimize_mean
  this->log(tr("Optimize")+" ");
  auto r = optimize_means( [this](){return this->isInterruptionRequested();} ,  values,group_sizes );
  this->log(tr("OK")+"\n");


  //log results
  auto  yes_no=[](bool b)->QString{
      if(b){return tr("yes");}
      else {return tr("no");}
  };
  this->log(
    "\n--- "+tr("Result summary")+" ---\n"
    +tr("mean of all values = %1").arg(r.reference_mean)+"\n"
    +tr("number_of_iterations = %1").arg (r.number_of_iterations)+"\n"
    +tr("interrupted = %1").arg(yes_no(r.interrupted))+"\n"
   );


  //write output
  {
      auto out = std::ofstream(out_folder+"/plan.tsv");
      out << "value\tgroup_id\tid\n";
      iterate_groups(values, group_sizes, [&](size_t gid, values_it b, values_it e){
        for( ; b!=e; ++b ){
            auto tmp = id_map.find(*b);
            std::string id = tmp->second.back();
            tmp->second.pop_back();
          out << *b << "\t" << gid << "\t" << id << "\n";
        }
      });
  }
  
  //write group stats
  {
      auto out = std::ofstream(out_folder+"/groupstats.tsv");
      out << "group_id\tsize\tmean\tsd\n";
      
      iterate_groups(values, group_sizes, [&](size_t gid, const values_it b, const values_it e){   
        size_t     size    = std::distance(b,e);
        value_type mean_v  = mean<value_type>     (b,e,size);
        value_type sd     =  sd_sample<value_type>(b,e,mean_v,size);
        out << gid << "\t" <<size << "\t" << mean_v << "\t" << sd <<"\n";
      });
  }
  
  //write pariwise t_test
  {
          auto out = std::ofstream(out_folder+"/pairwise_p.tsv");
          double worse_p = 2;
          
          //header: group_ids
          for(size_t i = 0; i < group_sizes.size(); ++i){
            out<<"\t"<<i;
          }out<<"\n";
          
          auto ba = values.begin();
  	  for(size_t group_ida = 0; group_ida < group_sizes.size() ; ++ group_ida){
     	    auto ea = ba + group_sizes[group_ida];
     	    
     	    auto bb = values.begin();
     	    out << group_ida;
     	    for(size_t group_idb = 0; group_idb < group_sizes.size() ; ++ group_idb){
     	        auto eb = bb + group_sizes[group_idb];
     	        
     	        if(group_idb<group_ida){
		  //already computed : can skip
     	          //out << "\tx";
     	          auto student = student_test(ba,ea,bb,eb);
     	          out <<"\t"<< student.p;
     	        }else{
     	          auto student = student_test(ba,ea,bb,eb);
     	          out <<"\t"<< student.p;
     	          if(student.p < worse_p){worse_p=student.p;}
     	        }
     	        
     	        bb=eb;
     	    }
	    out << "\n";
    	    
     	    ba=ea;
     	  }
     	  
     	  //write worst p_value
          //std::cout << "worse_p_value = "<< worse_p << std::endl;
          auto out2 = std::ofstream(out_folder+"/pairwise_p.tsv");
     	  out2 << "worse_p_value\n"<<worse_p; 
  }



  this->log(tr("Everything OK")+"\n");
  
}



void  Thread_optimize_mean::log(const QString &s){
    QObject * q = static_cast<QMainWindow  *>(&main_window);
    QApplication::instance()->postEvent(q, new Thread_log_event(s) );
}
