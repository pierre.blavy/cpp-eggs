#ifndef OPTIMIZE_MEAN_PIERRE
#define OPTIMIZE_MEAN_PIERRE

#include <thread>
#include<type_traits>
#include<algorithm>
#include<string>
#include<stdexcept>
#include <math.h>
#include<random>

template<typename T, typename S>  
struct Group_t{
    typedef T float_type;
    typedef S size_type;
    
    Group_t()=default;
    //Group_t(std::vector< float_type >::iterator b_, std::vector< float_type >::iterator e_, size_t s_):b(b_),e(e_),s(s_){}

    typename std::vector< float_type >::iterator b;  //points to the begin of the group, in values
    typename std::vector< float_type >::iterator e;  //points to the end   of the group, in values
    size_type size;
    
    float_type sum()const{float_type r=0; for(auto x = b; x !=e; ++x){r+=*x;} return r;  }
    
    float_type delta; 
    //positive value how much too high the group is
    //negative value how much too low  the group is
    //delta = group_sum - (group_size * reference_mean)
    
};




template <typename T>
auto abs_t(const T t){
  if constexpr (std::is_integral_v<T>){return abs(t);}
  else if constexpr (std::is_floating_point_v<T>){return fabs(t);}
  else{
    if(t<0){return -t;}
    else{return t;}
  }
}


//Try to permute values between two groups
//  Values are permuted if it makes an improvment, which means tha the worse group between ga and gb, is better.
//  return true on change, false if nothing is permuted
//  on permute, delta values are recomputed to reflect the change.
template<typename T, typename S>  
bool try_permute(Group_t<T,S> &ga, T& ea, Group_t<T,S>& gb, T& eb){
  auto permute_delta = ea - eb;
  auto new_delta_a   = ga.delta - permute_delta;
  auto new_delta_b   = gb.delta + permute_delta;
    
  bool permute = std::max( abs_t(new_delta_a), abs_t(new_delta_b) ) < std::max( abs_t(ga.delta), abs_t(gb.delta) ) ;
  if(permute){
    ga.delta -= permute_delta;
    gb.delta += permute_delta;
    std::swap(ea, eb);
  }
  
  return permute;
}


template<typename T> 
struct Optimize_means_result{
  typedef T value_t;
  value_t  reference_mean;  //mean of all values
  size_t number_of_iterations=0;
  bool interrupted=false;
};


template<typename Container_v, typename Container_s, typename Fn_stop >
Optimize_means_result<typename Container_v::value_type> 
optimize_means(
  Fn_stop stop_fn,                 //typically []()->bool{return stop_loop.stop_requested();}
  Container_v &values, 		       //values, like egg_weight. Floating point numbers. Values are reordered inplace
  const Container_s group_sizes,   //sizes of the groups. 

  bool check_size     = true,
  bool random_shuffle = true
){
  //NOTE : in this algorithm, values are swapped in place.
  //You must NEVER realloc values, as it will break the iterators


  typedef typename Container_v::value_type value_t;
  typedef typename Container_s::value_type group_size_t; 
  
  static_assert(std::is_floating_point_v<value_t>, "The values must be floating point numbers" );
  static_assert(std::is_integral_v<group_size_t>,       "The group_sizes must be integral numbers" );
  
  typedef Group_t<value_t,group_size_t> Group;


  
  
  //--- preconditions ---
  if(check_size){
    //all group_sizes must be >= 2
    for(const auto &s : group_sizes){
      if(s<2){throw std::runtime_error("Wrong group size : sixe must be >=2, it's actually " + std::to_string(s));}
    }
  
    //size_missmatch => throw
    //will be checked latter on, in the Create vec_groups section
  }
  

  //--- run the algorithm : prepare ---
  Optimize_means_result<value_t> r;


  //shuffle input
  if(random_shuffle){
    std::default_random_engine e;
    std::shuffle(values.begin(), values.end(), e);
  }
  
  //Create vec_group
  std::vector<Group>  vec_groups;
  vec_groups.resize( group_sizes.size() );
  {
    auto b = values.begin();
    for(size_t i = 0; i < group_sizes.size(); ++i){
      Group &g=vec_groups[i];
      g.size=group_sizes[i];
      g.b=b;
      b+=g.size;
      g.e=b;
    }
  
  }

  if(vec_groups.back().e != values.end() ){
     throw std::runtime_error(
       "Size mismatch, sum of group_size="+std::to_string(std::distance(vec_groups.front().b,  vec_groups.back().e ))
        + ", values_size="+std::to_string(values.size() )
      ) ;
  };
 
 
  //groups in search_worse will be ordered with the worse absolute value of delta first
  //delta = group_sum - group_size*reference_mean
  //the reference mean is the mean of all values
  
  std::vector<Group*> search_worse;   
  search_worse.reserve(vec_groups.size());
  for(Group &g:vec_groups){
    search_worse.push_back(&g);
  }
  
  //groups in search_delta will be ordered with the smallest abs( worse->delta + this_group.delta) first
  std::vector<Group*> search_delta;   
  search_delta.reserve(vec_groups.size());
  for(Group &g:vec_groups){
    search_delta.push_back(&g);
  }

  //compute reference_mean
  r.reference_mean = std::accumulate(values.begin(), values.end() , value_t(0) ) ;
  r.reference_mean/=values.size() ;
  
  //compute delta
  for(Group & g : vec_groups){
    g.delta = g.sum()  - r.reference_mean*g.size;
  }
  
  

  //--- run the algorithm : loop ---
  // loop_begin:
  // stop requested    => goto loop_end
  // compute stuff
  // something changed => goto loop_begin
  // loop_end:  
  
  loop_begin:
  if( stop_fn() ){
    r.interrupted=true;
    goto loop_end;
  }
  ++r.number_of_iterations;
  
  //sort search_worse vector : the worse group comes first
  std::sort(search_worse.begin(), search_worse.end(), [](const Group *a, const Group *b){
    //true a is worse than b
    return abs_t(a->delta)> abs_t(b->delta);
  } );
  
  for(Group * worse_group : search_worse){
  
    std::sort(search_delta.begin(), search_delta.end(), [&](const Group *a, const Group *b){
      //true a is worse than b
      auto da = abs_t(a->delta + worse_group->delta );
      auto db = abs_t(b->delta + worse_group->delta );
      return da < db;
    } );
  
    for(auto ea = worse_group->b; ea != worse_group->e; ++ea){
        
        if(worse_group->delta > 0 ){
          //if the group is too heavy, find and heavy egg
          if( *ea < r.reference_mean ){continue;}
        }else{
          //if the group is too light, find a light egg
          if( *ea  > r.reference_mean ){continue;}
        }

        //try to impove by using best quandidates
        //on improvment => goto loop_begin
        for(Group* gb : search_delta){
        for(auto eb = gb->b;  eb != gb->e; ++eb){
          //Test all eggs, if succes => goto something_changed
          bool something_changed =  try_permute(*worse_group, *ea ,*gb,* eb  );
          if(something_changed){goto loop_begin;}
        }}

     }
  }
  
  //nothing changed 
  loop_end:
  
  
  
  //second loop : bruteforce
  //try all possible permutations
  /*
  bool changed=false;
  do{
    if(stop_loop.stop_requested()){break;}
    ++r.number_of_iterations;
    
    for( size_t i = 0; i < vec_groups.size() ; ++ i ){
    for( size_t j = i; j < vec_groups.size() ; ++ j ){
      for( auto va = vec_groups[i].b;  va != vec_groups[i].e; ++va){
      for( auto vb = vec_groups[j].b;  vb != vec_groups[j].e; ++vb){
        changed |= try_permute(vec_groups[i], *va , vec_groups[j], *vb);
      }}
    }}

  }while(changed);
  */

  return r;
}

















/*
template<typename T>  
struct Egg{
    typedef T float_type;
    Egg(const std::string &s):id(s){}
    std::string id;
    float_type value;
};
*/



  



    





#endif

