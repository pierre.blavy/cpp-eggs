#include "widgets/MainWindow.hpp"

#include <QApplication>
#include <QLocale>
#include <QTranslator>

#include <config/config.hpp>
#include <config/convert/convert_QString.hpp>

#include <iostream>



int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator translator;

    config::Config conf;
    conf.load("in/config.txt");

    //configure language
    const QString lang = conf.blocks.get_unique("general").values.get_unique<QString>("language");
    const QString translation_path = "translations/"+lang+".qm";
    bool tr_ok = translator.load(translation_path);
    if(tr_ok){
        std::cout << "Using translation "<< translation_path.toStdString() <<std::endl;
        a.installTranslator(&translator);
    }else{
        std::cerr << "Cannot find translation : "<< translation_path.toStdString() <<std::endl;
    }

    //configure database
    MainWindow w( conf);
    w.translate();

    w.setStyleSheet("color:#000000");

    w.show();
    return a.exec();
}



