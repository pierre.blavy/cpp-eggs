QT += widgets charts
  # charts : sudo dnf install mingw32-qt6-qtcharts mingw64-qt6-qtcharts qt6-qtcharts-devel



CONFIG += icu
  #important windows doesn't support timezones correctly (most timezones are lacking, and the exiting ones are buggy)
  #this setting uses libicu to avoid this problem
TEMPLATE=app

#Translate
#  /usr/lib64/qt6/bin/lupdate ./eggs-gui.pro
#  /usr/lib64/qt6/bin/linguist  translations/fr.ts
#  /usr/lib64/qt6/bin/lrelease ./eggs-gui.pro

TRANSLATIONS += \
    translations/en.ts \
    translations/fr.ts

copy_files.commands += echo post link stuff; \
    $${QMAKE_MKDIR} $$shell_path($${OUT_PWD}/translations); \
    $(COPY_FILE)    $$shell_path($$PWD/translations/*.qm)  $$shell_path($${OUT_PWD}/translations/) ; \
    $${QMAKE_MKDIR} $$shell_path($${OUT_PWD}/in); \
    $(COPY_FILE)    $$shell_path($$PWD/in/*)  $$shell_path($${OUT_PWD}/in/) ; \
    $${QMAKE_MKDIR} $$shell_path($${OUT_PWD}/out); \


QMAKE_EXTRA_TARGETS += copy_files
QMAKE_POST_LINK     += make copy_files

CONFIG += qt -std=c++23

QMAKE_CXXFLAGS += -O0 -g

INCLUDEPATH += ./lib


# Dependancies
#linux :
#win32 : dnf -y install  mingw32-boost mingw32-boost-static
#win64 : dnf -y install  mingw64-boost mingw64-boost-static

HEADERS += \
./lib/read_csv.hpp \
./lib/stats/student.hpp \
./lib/stats/mean_sd.hpp \
./lib/tuple_tools/cat_integer_sequence.hpp \
./lib/tuple_tools/cat_tuples.hpp \
./lib/tuple_tools/filter_integer_sequence.hpp \
./lib/tuple_tools/is_integer_sequence.hpp \
./lib/tuple_tools/is_tuple.hpp \
./lib/tuple_tools/select_tuple.hpp \
./lib/tuple_tools/select_tuple_elements.hpp \
./lib/Thread_optimize_mean.hpp \
./lib/optimize_mean.hpp \
./lib/widgets/MainWindow.hpp \
./lib/widgets/IsOk.hpp \
./lib/widgets/Filepath.hpp \
./lib/config/config.hpp \
./lib/config/convert/convert_QString.hpp \
./lib/config/convert/convert_bool.hpp \
./lib/config/convert/convert_chrono.hpp \
./lib/config/convert/convert_id.hpp \
./lib/config/convert/convert_lexical.hpp \
./lib/config/convert/convert_path.hpp \
./lib/config/convert/convert_unit_lite.hpp \
./lib/config/convert/string_cstring.hpp \
./lib/config/get_multiline.hpp \
./lib/container/array.hpp \
./lib/container/container.hpp \
./lib/container/deque.hpp \
./lib/container/impl/Complexity.hpp \
./lib/container/set.hpp \
./lib/container/unordered_set.hpp \
./lib/container/vector.hpp \
./lib/convert/QDateTime_to_string.hpp \
./lib/convert/convert.hpp \
./lib/convert/from_QString.hpp \
./lib/convert/string_cstring.hpp \
./lib/convert/to_QString.hpp \
./lib/convert/to_string.hpp \
./lib/convert/QDateTime_to_QString.hpp \
./lib/qtt/qtt.hpp \
./lib/qtt/qtt_Translatable.hpp \
./lib/qtt/qtt_icon_or_text.hpp \
./lib/qtt/qtt_layout.hpp \
    lib/stats/boxplot.h \
    lib/stats/quantiles.hpp \
    lib/widgets/BoxPlots.hpp



SOURCES += \
./lib/widgets/MainWindow.cpp \
./lib/widgets/IsOk.cpp \
./lib/widgets/Filepath.cpp \
./lib/config/config.cpp \
./lib/container/test/array.cpp \
./lib/container/test/deque.cpp \
./lib/container/test/set.cpp \
./lib/container/test/unordered_set.cpp \
./lib/container/test/vector.cpp \
./main.cpp \
./lib/Thread_optimize_mean.cpp \
./lib/widgets/BoxPlots.cpp


LIBS += \
# -lz  \
# -lsqlite3  \

unix {
  LIBS += \
  -lpthread \
  #-ldl  \
}


#=== WIN64 dll ===
win32{ #win32 means ANY windows
  contains(QT_ARCH, x86_64) {

    QMAKE_POST_LINK += ; echo "--- WIN64 copy dll ---";
    QMAKE_POST_LINK += $(COPY_DIR) $$shell_path("/usr/x86_64-w64-mingw32/sys-root/mingw/lib/qt6/plugins/platforms")  $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/iconv.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icudata74.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icui18n74.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/icuuc74.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libbz2-1.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libexpat-1.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfontconfig-1.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libfreetype-6.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libglib-2.0-0.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libharfbuzz-0.dll" $$shell_path($${OUT_PWD}/release/);


    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libgcc_s_seh-1.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libintl-8.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-8-0.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpcre2-16-0.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libpng16-16.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libstdc++-6.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/libwinpthread-1.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/zlib1.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Core.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Gui.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Widgets.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Charts.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6OpenGL.dll" $$shell_path($${OUT_PWD}/release/);
    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6OpenGLWidgets.dll" $$shell_path($${OUT_PWD}/release/);

    QMAKE_POST_LINK += $(COPY_FILE) "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/Qt6Charts.dll" $$shell_path($${OUT_PWD}/release/);
    
  }
}

