#!/bin/sh

#WARNING
#don't  upx platforms/*.dll
#don't  upx libgcc_s_dw2-1.dll


upx_decomprss(){
  if [ "$upx" = "yes" ]
  then
    if [ -f "$1" ];  then upx -d "$1"; fi 
  fi
}

upx_compress (){
  if [ "$upx" = "yes" ];  then upx $1; fi 
}



win_any() {
	echo "--- win32 BEGIN ---";
	local BITS=$1
	local PREFIX=$2

	local OUT_PATH="bin-win${BITS}"  #don't add a / afer OUT_PATH, as it will be used as a zip basename

	rm -f "${OUT_PATH}.zip" ;
	
	rm -rf "$OUT_PATH"     &&\
	mkdir -p "$OUT_PATH"  &&\
	cd "$OUT_PATH"  &&\
	echo `pwd` &&\
	"${PREFIX}-qmake-qt6" ../  &&\
	make -j$(nproc) release &&\
	make clean  &&\
	rm -f Makefile  &&\
	rm -f Makefile.Debug  &&\
	rm -f Makefile.Release  &&\
	mv release/* ./   &&\
	upx_compress "*.exe" &&\
	upx_compress "*.dll" &&\
	upx_decomprss libgcc_s_dw2-1.dll &&\
	upx_decomprss libgcc_s_seh-1.dll &&\
	rm -r ./release &&\
	rm -r ./debug &&\
	cd - &&\
	zip -rm9 "$OUT_PATH.zip" "$OUT_PATH" &&\
	echo "--- win${BITS} OK ---";
}


#upx="no"
#win_any 32 i686-w64-mingw32


upx="no"  #upx is detected as a virus
win_any 64 x86_64-w64-mingw32


