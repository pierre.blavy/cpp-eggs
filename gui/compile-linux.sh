#!/bin/sh

OUT_PATH='bin-linux/'

rm -rf "$OUT_PATH"     &&\
mkdir -p "$OUT_PATH"  &&\
cd "$OUT_PATH"  &&\
qmake ../  &&\
make -j$(nproc) &&\
make clean  &&\
rm Makefile

zip -rm9 "$OUT_PATH.zip" "$OUT_PATH"
