<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="fr">
<context>
    <name>Filepath</name>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="36"/>
        <source>Browse...</source>
        <translation>Parcourir...</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="56"/>
        <source>Chose a file that exists</source>
        <translation>Choisissez un fichier existant</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="57"/>
        <source>Choose a new file</source>
        <translation>Choisissez un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="58"/>
        <source>Choose a file</source>
        <translation>Choisissez un fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="62"/>
        <source>Chose a directory that exists</source>
        <translation>Choisissez un répertoire qui existe</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="63"/>
        <source>Choose a new directory</source>
        <translation>Choisissez un nouveau répertoire</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="64"/>
        <source>Choose a directory</source>
        <translation>Choisissez un répertoire</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="96"/>
        <source>A regular file is expected</source>
        <translation>Le programme attend un fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="97"/>
        <source>A directory is expected</source>
        <translation>Le programme attend un répertoire</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="101"/>
        <source>The file must exist</source>
        <translation>Vous devez choisir un fichier qui existe</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="102"/>
        <source>The file must NOT exist</source>
        <translation>Vous devez choisir un nouveau nom de fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="103"/>
        <source>The file already exist</source>
        <translation>Le fichier existe déjà</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="107"/>
        <source>The directory must exist</source>
        <translation>Vous devez choisir un répertoire qui existe</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="108"/>
        <source>The directory must NOT exist</source>
        <translation>Vous devez choisir un nouveau nom de répertoire, pas un répertoire qui existe déjà</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="109"/>
        <source>The directory already exist</source>
        <translation>Le répertoire existe déjà</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="132"/>
        <source>A regular file that exists is expected.</source>
        <translation>Le programme attends le nom d&apos;un fichier qui existe déjà</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="133"/>
        <source>A regular file that do NOT exist is expected.</source>
        <translation>Le programme attends le nom d&apos;un nouveau fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="134"/>
        <source>A regular file is expected.</source>
        <translation>Le programme attends un fichier</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="136"/>
        <source>File path:</source>
        <translation>Chemin du fichier:</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="140"/>
        <source>A directory that exists is expected.</source>
        <translation>Le programme attend un répertoire qui existe déja</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="141"/>
        <source>A directory that do NOT exist is expected.</source>
        <translation>Le programme attend un nouveau répertoire</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="142"/>
        <source>A directory is expected.</source>
        <translation>Le programme attend un répertoire</translation>
    </message>
    <message>
        <location filename="../lib/widgets/Filepath.cpp" line="143"/>
        <source>Directory path:</source>
        <translation>Chemin du répertoire:</translation>
    </message>
</context>
<context>
    <name>MainWindow</name>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="72"/>
        <source>Input</source>
        <translation>Entrées</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="73"/>
        <source>Values</source>
        <translation>Valeurs</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="74"/>
        <source>Groups</source>
        <translation>Groupes</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="76"/>
        <source>Output</source>
        <translation>Sorties</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="77"/>
        <source>Out folder</source>
        <translation>Répertoire de sortie</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="79"/>
        <source>&amp;Run</source>
        <comment>Keep R as shortcut</comment>
        <translation>Lance&amp;r</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="81"/>
        <source>Result</source>
        <translation>Résultats</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="89"/>
        <source>Please wait...</source>
        <translation>Attendez svp...</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="94"/>
        <source>Starting...</source>
        <translation>Lancement ...</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="101"/>
        <source>Interrupt</source>
        <translation>Interrompre le calcul</translation>
    </message>
    <message>
        <location filename="../lib/widgets/MainWindow.cpp" line="107"/>
        <source>Run again</source>
        <translation>Re lancer le calcul</translation>
    </message>
</context>
<context>
    <name>Thread_optimize_mean</name>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="47"/>
        <source>ERROR:
</source>
        <translation>ERREUR:</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="49"/>
        <source>ERROR : unknown error
</source>
        <translation>ERREUR : erreur inconue</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="72"/>
        <source>Read values</source>
        <translation>Lecture des valeurs</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="78"/>
        <location filename="../lib/Thread_optimize_mean.cpp" line="96"/>
        <source>OK</source>
        <translation>OK</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="82"/>
        <source>Read groups</source>
        <translation>Lecture des groupes</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="93"/>
        <source>Optimize</source>
        <translation>Opimisation</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="101"/>
        <source>yes</source>
        <translation>oui</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="102"/>
        <source>no</source>
        <translation>non</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="105"/>
        <source>Result summary</source>
        <translation>Résumé des résultats</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="106"/>
        <source>mean of all values = %1</source>
        <translation>moyenne de toutes les valeurs %1</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="107"/>
        <source>number_of_iterations = %1</source>
        <translation>nombre d&apos;itérations de l&apos;algo = %1</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="108"/>
        <source>interrupted = %1</source>
        <translation>L&apos;algo a été interrompu = %1</translation>
    </message>
    <message>
        <location filename="../lib/Thread_optimize_mean.cpp" line="179"/>
        <source>Everything OK</source>
        <translation>Tout est terminé, OK</translation>
    </message>
</context>
</TS>
