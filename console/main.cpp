


#include <stats/student.hpp>
#include <read_csv.hpp>

#include <boost/lexical_cast.hpp>

#include <iostream>
#include <vector>
#include <random>
#include <algorithm>
#include <stop_token>
#include <filesystem>	
#include <type_traits>

#include <math.h>
#include <signal.h>


typedef double Egg_weight ;
  
struct Egg{
    Egg(const std::string &s):id(s){}
    std::string id;
    Egg_weight value;
};

struct Group_stats{
  double mean=0;
  double sd=0;
};
  
struct Group{
    explicit Group(size_t s):size(s){}
    std::vector<Egg>::iterator b;
    std::vector<Egg>::iterator e;
    size_t size;
    
    Egg_weight sum()const{Egg_weight r=0; for(auto x = b; x !=e; ++x){r+=x->value;} return r;  }
    
    Egg_weight delta; 
    //positive value how much too heavy the group is
    //negative value how much too light the group is
    
    Group_stats stats()const{
      Group_stats r;
      for(auto x = b; x!=e; ++x){
        r.mean +=x->value;
      }
      r.mean/=size;

      //sample sd
      for(auto x = b; x!=e; ++x){
        double tmp= ( r.mean - x->value );
        r.sd += tmp*tmp;
      }
      r.sd/=(size-1);
      r.sd=sqrt(r.sd);
      return r;
    }
    
    //TODO this is not efficient, use adaptor instead
    std::vector<double> to_values()const{
      std::vector<double> r;
      r.reserve(size);
      std::transform(b,e, std::back_insert_iterator(r), [](const Egg &e){return e.value;} );
      return r;
    }
    

};
    
    
template <typename T>
auto abs_t(const T t){
  if constexpr (std::is_integral_v<T>){return abs(t);}
  else if constexpr (std::is_floating_point_v<T>){return fabs(t);}
  else{
    if(t<0){return -t;}
    else{return t;}
  }
}


//HERE ga must be the worse group, if you change that
//change the line worse_delta=???
bool try_permute(Group &ga, Egg& ea, Group& gb, Egg & eb){
  auto permute_delta = ea.value - eb.value;
  auto new_delta_a   = ga.delta - permute_delta;
  auto new_delta_b   = gb.delta + permute_delta;
  
  //auto worse_delta = abs_t(ga.delta);
  
  bool permute = std::max( abs_t(new_delta_a), abs_t(new_delta_b) ) < std::max( abs_t(ga.delta), abs_t(gb.delta) ) ;
  if(permute){
    ga.delta -= permute_delta;
    gb.delta += permute_delta;
    std::swap(ea, eb);
  }
  
  return permute;
}


//--- stop on control + C ---
std::stop_source stop_source;
void stop(int){
  stop_source.request_stop();
}



int main(int,char**){

  signal(SIGINT, &stop);
  

  std::vector<Egg>    vec_eggs;
  std::vector<Group>  vec_groups;

  //this function will save the results
  auto finish=[&](){
    std::filesystem::create_directory("out");

    {//save experiment plan
        auto out = std::ofstream("out/plan.tsv");
        size_t gid = 0;
        out << "group_id\tvalue_id\tvalue";
        for(const auto &g : vec_groups){
          ++gid;
          for(auto x = g.b; x!= g.e; ++x){
             out <<"\n"<< gid << "\t" << x->id << "\t" << x->value;
          }
        }
    }
    
    //TODO use adapters instead
    std::vector<double> egg_values;egg_values.reserve(vec_eggs.size() );
    std::transform(vec_eggs.begin(), vec_eggs.end(), std::back_insert_iterator(egg_values), [](const Egg &e){return e.value;} );

    {//save groups statistics
        auto out = std::ofstream("out/group_stats.tsv");
        size_t gid = 0;
        out << "group_id\tgroup_size\tgroup_mean\tgroup_sd\tgroup_p";
        for(const auto &g : vec_groups){
          ++gid;
          auto st = g.stats();
          auto student = student_test(egg_values, g.to_values() );
          out <<"\n"
            << gid << "\t" 
            << g.size << "\t"
            << st.mean << "\t" 
            << st.sd <<"\t" 
            << student.p  ;
        }
    }
    
    //save pair t_test
    {
        auto out = std::ofstream("out/pairwise_p.tsv");
        //precompute vectors of values
        //TODO optimize, use adaptors instead
        std::vector<std::vector<double> > gv;
        gv.reserve(vec_groups.size() );
        for(const Group &g : vec_groups){gv.push_back(g.to_values());}
        
        //header
        for(size_t i = 0; i < gv.size(); ++i){
          out<<"\t"<<i+1;
        }out<<"\n";
        
        //data
        double worse_p = 2;
        for(size_t i = 0; i < gv.size(); ++i){
          out << i+1;
          for(size_t j = 0; j < gv.size(); ++j){
             auto student = student_test(gv[i], gv[j] );
             out << "\t" << student.p;
             if(student.p < worse_p){worse_p=student.p;}
          }
          out<<"\n";
        }
        
        std::cout << "worse pairwise p " << worse_p << std::endl;
    }
 
  };


  {// parse values.txt  
    auto rs = read_csv(
      "id",     [&](const std::string &s){vec_eggs       .push_back(s);},
      "value",  [&](const std::string &s){vec_eggs.back().value = boost::lexical_cast<double>(s);}
    );
    rs.read("in/values.txt");
    if(vec_eggs.size()==0){throw std::runtime_error("no values, check in/values.txt");}
  }
  
  {// parse groups.txt  
    auto rs = read_csv(
      "group_size",  [&](const std::string &s){vec_groups.emplace_back(boost::lexical_cast<size_t>(s));}
    );
    rs.read("in/groups.txt");
    if(vec_groups.size()==0){throw std::runtime_error("no groups, check in/groups.txt");}

  }
  

  
  //groups ordered with the worse absolute value of delta first
  std::vector<Group*> search_worse;   
  search_worse.reserve(vec_groups.size());
  for(Group &g:vec_groups){
    search_worse.push_back(&g);
  }
  
  //groups ordered with the smallest abs( worse->delta + this_group.delta) first
  std::vector<Group*> search_delta;   
  search_delta.reserve(vec_groups.size());
  for(Group &g:vec_groups){
    search_delta.push_back(&g);
  }
  
  
    
  //randomly shuffle eggs
  std::default_random_engine e;
  std::shuffle(vec_eggs.begin(), vec_eggs.end(), e);
  
  //FROM HERE : never realloc  in vec_eggs
  //it will break iterators  
  //compute groups iterators
  {
    auto b = vec_eggs.begin();
    for(Group &g : vec_groups){
      g.b=b;
      b+=g.size;
      g.e=b;
   }
   if(vec_groups.back().e != vec_eggs.end() ){
     throw std::runtime_error(
       "Size mismatch, group_size="+std::to_string(std::distance(vec_groups.front().b,  vec_groups.back().e ))
        + ", values_size="+std::to_string(vec_eggs.size() )
      ) ;
   };
  }
  
  
  //compute delta
  double average_egg = 0;
  for(const auto &e : vec_eggs){average_egg+=e.value;}
  average_egg/=vec_eggs.size() ;
  std::cout << "average value " << average_egg << std::endl;
  
  for(Group & g : vec_groups){
    g.delta = g.sum()  - average_egg*g.size;
  }
  
  
  std::stop_token stop_loop = stop_source.get_token();
  
  size_t loop_count = 0;
  loop_begin:
  if(stop_loop.stop_requested()){
    std::cout << "INTERRUPTED" <<std::endl;
    goto loop_end;
  }
  ++loop_count;
  
  //sort search_worse vector : the worse group comes first
  std::sort(search_worse.begin(), search_worse.end(), [](const Group *a, const Group *b){
    //true a is worse than b
    return abs_t(a->delta)> abs_t(b->delta);
  } );
  
  for(Group * worse_group : search_worse){
  
    std::sort(search_delta.begin(), search_delta.end(), [&](const Group *a, const Group *b){
      //true a is worse than b
      auto da = abs_t(a->delta + worse_group->delta );
      auto db = abs_t(b->delta + worse_group->delta );
      return da < db;
    } );
  
    for(auto ea = worse_group->b; ea != worse_group->e; ++ea){
        
        if(worse_group->delta > 0 ){
          //if the group is too heavy, find and heavy egg
          if( ea->value  < average_egg ){continue;}
        }else{
          //if the group is too light, find a light egg
          if( ea->value  > average_egg ){continue;}
        }

        //try to impove by using best quandidates
        //on improvment => goto loop_begin
        for(Group* gb : search_delta){
        for(auto eb = gb->b;  eb != gb->e; ++eb){
          //Test all eggs, if succes => goto something_changed
          bool something_changed =  try_permute(*worse_group, *ea ,*gb,* eb  );
          if(something_changed){goto loop_begin;}
        }}

     }
  }
  
   //nothing changed 
   loop_end:
   finish();
   std::cout << "OK after " << loop_count << " iterations" << std::endl;

}


