#!/bin/sh

#dnf install mingw64-gcc-c++ mingw64-boost-static mingw64-boost
rm -rf win64
rm -rf win64.zip 

mkdir -p win64

cp -r -f in win64/
cp -r -f R  win64/

mkdir -p win64/src

cp -f *.hpp win64/src/
cp -f *.cpp win64/src/
cp -f *.sh  win64/src/
cp -r  R    win64/src/
cp -r  lib  win64/src/

x86_64-w64-mingw32-g++ -O3  -Ilib -std=c++20 -o win64/eggs.exe ./main.cpp



cpdll(){
  cp -f  "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/$1" "win64/"
}


cpdll_u(){
  cp -f  "/usr/x86_64-w64-mingw32/sys-root/mingw/bin/$1" "win64/"
  upx --best "win64/$1"
}


upx eggs.exe
cpdll_u libgcc_s_seh-1.dll
cpdll_u libstdc++-6.dll
cpdll_u libwinpthread-1.dll



cat <<EOF > win64/eggs_cmd.bat
cd /d %~dp0
eggs.exe; 
pause
EOF

chmod +x win64/eggs_cmd.bat

zip -rm9 win64.zip ./win64



