#ifndef STUDENT_PIERRE_HPP
#define STUDENT_PIERRE_HPP

#include <boost/math/distributions/students_t.hpp>
#include <numeric>

// DOC : 
//   https://live.boost.org/doc/libs/1_48_0/libs/math/doc/sf_and_dist/html/math_toolkit/dist/stat_tut/weg/st_eg/two_sample_students_t.html
// Doc details :
//   https://beta.boost.org/doc/libs/1_53_0/libs/math/doc/sf_and_dist/html/math_toolkit/dist/dist_ref/dists/students_t_dist.html
//   https://www.boost.org/doc/libs/1_72_0/boost/math/distributions/students_t.hpp
//   https://www.bibmath.net/dico/index.php?action=affiche&quoi=./s/studenttest.html
//   https://www.datanovia.com/en/fr/lessons/formule-du-test-t/formule-du-test-t-independant/
//   https://live.boost.org/doc/libs/1_48_0/libs/math/example/students_t_two_samples.cpp
//   https://www.itl.nist.gov/div898/handbook/eda/section3/eda353.htm



struct Student_result{
  double df;  //degree of freedom
  double t;   //t test value
  double p;   //p value (bilateral)
};


namespace student_impl{
struct Nmv{
  size_t n;      //number of points
  double mean;   //mean
  double var;    //sd²
  double varn;   //sd²/n
};

template<typename It_begin, typename It_end>
Nmv nmv( It_begin b, It_end e){
  Nmv r;

  r.n    = std::distance(b,e);
  r.mean = std::accumulate(b,e,0.0);r.mean/=r.n;
  r.var=0; for(auto i = b; i != e; ++i ){double tmp = *i - r.mean ;r.var+=tmp*tmp ;}r.var/=( r.n -1);
  r.varn=r.var/r.n ;
  
  return r;
}
}

template<typename It_Abegin, typename It_Aend, typename It_Bbegin, typename It_Bend >
Student_result student_test( It_Abegin a_begin, It_Aend a_end,  It_Bbegin b_begin, It_Bend b_end){
  using namespace student_impl;
  typedef boost::math::students_t_distribution<> students_t;
    
  Student_result r;

  Nmv xa = nmv(a_begin,a_end);
  Nmv xb = nmv(b_begin,b_end);

  double sum_varn = xa.varn + xb.varn;
  r.t  = (xa.mean-xb.mean)/sqrt( sum_varn );
   
  auto f=[](const Nmv &x){return (x.varn*x.varn)/(x.n-1)  ;};
  r.df = (sum_varn*sum_varn)/(f(xa) + f(xb)   );
  
  students_t x(r.df);
  r.p = 2* cdf(complement(x,fabs(r.t)) );
  return r;
}


template<typename A, typename B>
Student_result student_test( const A&a, const B&b ){
  return student_test(a.cbegin(), a.cend(), b.cbegin(), b.cend() );
}


#endif
