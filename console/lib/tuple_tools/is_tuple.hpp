#ifndef IS_TUPLE_PIERRE_HPP
#define IS_TUPLE_PIERRE_HPP

#include <tuple>

namespace tuple_tools{



template<typename T>    struct is_tuple{
  static constexpr bool value = false;
  is_tuple()=delete;
};

template<typename... A> struct is_tuple<std::tuple<A...> >{
  static constexpr bool value = true;
  is_tuple()=delete;
};



}



#endif
