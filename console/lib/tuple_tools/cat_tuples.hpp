#ifndef CAT_TUPLES_PIERRE_HPP
#define CAT_TUPLES_PIERRE_HPP

#include <tuple>

namespace tuple_tools{


template<typename ...A> struct cat_tuples;

//--- cat 0  ----
template<> 
struct cat_tuples<>{
  typedef std::tuple<> type;
};


//--- cat 1  ----
template<typename ...A> 
struct cat_tuples<std::tuple<A...>>{
  typedef std::tuple<A...> type;
};

template< typename A> 
struct cat_tuples<A >{
  //,  typename = std::enable_if<!Is_tuple<A>::value>::type
  typedef std::tuple<A> type;
};


//--- cat 2+ ---
template<typename ...A, typename ...B, typename ...C> 
struct cat_tuples<std::tuple<A...>, std::tuple<B...>, C...>{
  typedef  typename cat_tuples< std::tuple<A...,B...>, C...>::type type;
};


template<
  typename ...A, typename B, typename ...C
> 
struct cat_tuples<std::tuple<A...>, B, C...>{
  typedef typename cat_tuples< std::tuple<A...,B>, C...>::type type;
};

template<
  typename A, typename... B, typename ...C
> 
struct cat_tuples<A, std::tuple<B...> , C...>{
  typedef typename cat_tuples< std::tuple<A,B...>, C...>::type type;
};


template<
  typename A, typename B, typename ...C
> 
struct cat_tuples<A, B , C...>{
  typedef typename cat_tuples< std::tuple<A,B>, C...>::type type;
};



/*
namespace{
  [[maybe_unused]] void cat_tuples_test(){
    static_assert(std::is_same<cat_tuples<>::type, std::tuple<> >::value,"");
    static_assert(std::is_same<cat_tuples<int>::type, std::tuple<int> >::value,"");
    static_assert(std::is_same<cat_tuples<int,double>::type, std::tuple<int,double> >::value,"");
    static_assert(std::is_same<cat_tuples<int,double,std::tuple<char>>::type, std::tuple<int,double,char> >::value,"");
    static_assert(std::is_same<cat_tuples<std::tuple<int>,double,std::tuple<char>>::type, std::tuple<int,double,char> >::value,"");
    static_assert(std::is_same<cat_tuples<std::tuple<int>,double,std::tuple<std::tuple<char>>>::type, std::tuple<int,double,std::tuple<char>> >::value,"");
  }
}*/



}//end namespace tuple_tools


#endif
