#ifndef SELECT_TUPLE_ELEMENTS_PIERRE_HPP
#define SELECT_TUPLE_ELEMENTS_PIERRE_HPP

#include "cat_tuples.hpp"

namespace tuple_tools{


/*
namespace impl{
	template<typename indexes>
	struct Select_tuple_elements;

	template<size_t... I>
	struct Select_tuple_elements< std::integer_sequence<size_t,I...> >{
	  template<typename... A>
	  static auto run(A&&... a){
	    return  std::make_tuple( std::get< I  >( std::forward_as_tuple(a...) )... );
	  }
	};
}*/




template<typename Tuple_t, typename Sequence_t>
struct select_tuple_elements;


template<typename... A, typename T>
struct select_tuple_elements<std::tuple<A...> , std::integer_sequence<T>  >{
   typedef std::tuple<> type;
};


template<typename... A, typename T, T x, T...i>
struct select_tuple_elements< std::tuple<A...> , std::integer_sequence<T, x, i...>  >{
  typedef typename cat_tuples< 
    std::tuple< typename std::tuple_element<x,std::tuple<A...> >::type >, 
    typename select_tuple_elements<std::tuple<A...>,std::integer_sequence<T,i...> >::type 
  >::type type;
   
};



}
#endif
