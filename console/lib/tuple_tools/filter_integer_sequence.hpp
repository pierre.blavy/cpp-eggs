#ifndef FILTER_INTEGER_SEQUENCE_PIERRE_HPP
#define FILTER_INTEGER_SEQUENCE_PIERRE_HPP

#include "cat_integer_sequence.hpp"

namespace tuple_tools{


namespace impl{
  template<bool,typename A,typename B> struct static_if            {typedef A type;};
  template<     typename A,typename B> struct static_if<false,A,B> {typedef B type;};
}


template<typename Seq, auto Pred  > struct filter_integer_sequence;


//0
template<typename T,  auto Pred  >
struct filter_integer_sequence< std::integer_sequence<T>, Pred>{
  typedef std::integer_sequence<T> type;
};


//1+
template<typename T, T x, T...a, auto Pred  >
struct filter_integer_sequence< std::integer_sequence<T,x,a...> , Pred >{
  typedef typename impl::static_if< Pred(x) , std::integer_sequence<T,x> ,  std::integer_sequence<T>  >::type f_type;
  typedef typename cat_integer_sequence<f_type, typename filter_integer_sequence<  std::integer_sequence<T,a...> , Pred  >::type>::type type;
};


}

#endif
